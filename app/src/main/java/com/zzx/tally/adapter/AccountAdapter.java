package com.zzx.tally.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.zzx.tally.R;
import com.zzx.tally.beans.AccountBean;

import java.util.List;

public class AccountAdapter extends BaseAdapter {
    List<AccountBean> list;
    LayoutInflater inflater;

    public AccountAdapter(List<AccountBean> list, Context context) {
        this.list = list;
        this.inflater = LayoutInflater.from(context);
    }
    public AccountAdapter() {
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.indexOf(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view=inflater.inflate(R.layout.activity_mainlv,viewGroup,false);

        AccountBean bean=list.get(i);
        ImageView typeIv;
        TextView typeTv,beizhuTv,timeTv,moneyTv;
        typeIv = view.findViewById(R.id.item_mainlv_iv);
        typeTv = view.findViewById(R.id.item_mainlv_tv_title);
        timeTv = view.findViewById(R.id.item_mainlv_tv_time);
        beizhuTv = view.findViewById(R.id.item_mainlv_tv_beizhu);
        moneyTv = view.findViewById(R.id.item_mainlv_tv_money);
        typeIv.setImageResource(bean.getsImageId());
        typeTv.setText(bean.getTypename());
        beizhuTv.setText(bean.getBeizhu());
        moneyTv.setText("￥ "+bean.getMoney());
        timeTv.setText(bean.getTime());

        return view;
    }
}
