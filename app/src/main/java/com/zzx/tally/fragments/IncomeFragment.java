package com.zzx.tally.fragments;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.zzx.tally.R;
import com.zzx.tally.beans.TypeBean;
import com.zzx.tally.daos.DBManager;

import java.util.List;

public class IncomeFragment extends com.zzx.tally.fragments.BaseRecordFragment {


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        accountBean.setTypename("其他");
        accountBean.setsImageId(R.mipmap.in_qt_fs);
    }

    @Override
    public void loadDataToGV() {
        super.loadDataToGV();

        List<TypeBean> inlist = DBManager.getTypeList(1);
        typeList.addAll(inlist);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void saveAccountToDB() {
        accountBean.setKind(1);
        DBManager.insertItemToAccounttb(accountBean);
    }
}
