package com.zzx.tally.fragments;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.zzx.tally.R;
import com.zzx.tally.beans.TypeBean;
import com.zzx.tally.daos.DBManager;

import java.util.List;


public class OutcomeFragment extends com.zzx.tally.fragments.BaseRecordFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        accountBean.setTypename("其他");
        accountBean.setsImageId(R.mipmap.ic_qita_fs);
    }

    @Override
    public void loadDataToGV() {
        super.loadDataToGV();

        List<TypeBean> outlist = DBManager.getTypeList(0);
        typeList.addAll(outlist);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void saveAccountToDB() {
        accountBean.setKind(0);
        DBManager.insertItemToAccounttb(accountBean);
    }
}
