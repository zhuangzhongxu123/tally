package com.zzx.tally;

import android.app.Application;

import com.zzx.tally.daos.DBManager;


public class UniteApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        DBManager.initDB(getApplicationContext());
    }
}
