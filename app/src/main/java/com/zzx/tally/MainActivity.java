package com.zzx.tally;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.zzx.tally.adapter.AccountAdapter;
import com.zzx.tally.beans.AccountBean;
import com.zzx.tally.daos.DBManager;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    ListView lv;
    List<AccountBean> mDatas;
    AccountAdapter adapter;
    Button bt;
    ImageView imageView;
    ImageButton ibt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lv=findViewById(R.id.main_lv);
        mDatas=new ArrayList<>();
        adapter=new AccountAdapter(mDatas,this);
        lv.setAdapter(adapter);
        setOnLongClick();

        bt=findViewById(R.id.main_btn_edit);
        bt.setOnClickListener(this);

        imageView=findViewById(R.id.main_iv_search);
        imageView.setOnClickListener(this);

        ibt=findViewById(R.id.main_btn_more);
        ibt.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadDBData();
    }
    private void loadDBData() {
        List<AccountBean> list = DBManager.getAccountListOneDayFromAccounttb();
        mDatas.clear();
        mDatas.addAll(list);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.main_btn_edit:
                Intent it1 = new Intent(this, RecordActivity.class);
                startActivity(it1);
                break;
            case R.id.main_iv_search:
                Intent it2 = new Intent(this, SearchActivity.class);
                startActivity(it2);
                break;
            case R.id.main_btn_more:
                Intent it3 = new Intent(this, AboutActivity.class);
                startActivity(it3);
                break;
            default:
                break;
        }
    }
    void setOnLongClick(){
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                AccountBean clickBean = mDatas.get(position);  //获取正在被点击的这条信息

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("提示信息").setMessage("您确定要删除这条记录么？")
                        .setNegativeButton("取消",null)
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                int click_id = clickBean.getId();

                                DBManager.deleteItemFromAccounttbById(click_id);
                                mDatas.remove(clickBean);
                                adapter.notifyDataSetChanged();
                            }
                        });
                builder.create().show();

                return false;
            }
        });
    }

}

